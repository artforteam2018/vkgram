import 'package:flutter/material.dart';
import '../api.dart';

class LandingPage extends StatefulWidget {
  LandingPage({Key key, this.mainKey}) : super(key: key);
  static String tag = 'login-page';

  final mainKey;

  @override
  _LandingPageState createState() => new _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  Future logged;

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      onSaved: (String value) {
        widget.mainKey.currentState.setLoginData(value, null);
      },
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      onSaved: (String value) {
        widget.mainKey.currentState.setLoginData(null, value);
      },
      decoration: InputDecoration(
        hintText: 'Пароль',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
              style: BorderStyle.solid,
            )),
        onPressed: (() {
          !widget.mainKey.currentState.instaAuth
              ? widget.mainKey.currentState.setState(() {
            widget.mainKey.currentState.instaNeedAuth = true;
          })
              : widget.mainKey.currentState.setState(() {
            widget.mainKey.currentState.vkNeedAuth = true;
            widget.mainKey.currentState.flutterWebviewPlugin.launch(widget.mainKey.currentState.vkUrl);
            widget.mainKey.currentState.flutterWebviewPlugin
                .resize(Rect.fromLTWH(0.05, 0.05, MediaQuery.of(context).size.width, MediaQuery.of(context).size.height * 0.86));
          });


        }),
        padding: EdgeInsets.all(12),
        color: Colors.white,
        disabledColor: Colors.white70,
        child: Text('Начать', style: TextStyle(color: Colors.black)),
      ),
    );

    final loader = FutureBuilder<dynamic>(
        future: logged,
        // a Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return new Text('');
            case ConnectionState.waiting:
              return Image.asset(
                'assets/loader.gif',
                height: 16,
              );
            default:
              if (snapshot.hasError)
                return new SizedBox(
                    height: 16,
                    child: Text(
                      snapshot.error,
                      textAlign: TextAlign.center,
                    ));
              else
                return Text('');
          }
        });

    final infoLabel = Container(
      child: Text(
        'Внимание!\n'
            'Все данные сохраняются только на вашем устройстве. При переустановке приложения авторизироваться нужно будет заново.',
        style: TextStyle(color: Colors.black, fontSize: 14),
      ),
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(border: Border.all(style: BorderStyle.solid)),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Авторизация',
        style: TextStyle(color: Colors.black54, fontSize: 24),
      ),
      onPressed: () {},
    );

    final logoText = Text(
      'STORIES \n Instagram - Вконтакте',
      style: TextStyle(color: Colors.black54, fontSize: 24),
      textAlign: TextAlign.center,
    );

    final authIcons = Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
              width: 120,
              child: Column(children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 30.0,
                  child: Image.asset('assets/Instagram.png'),
                ),
                SizedBox(height: 10),
                Text(
                  'Аккаунт Instagram',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: widget.mainKey.currentState.instaAuth ? Color(0xFF00BB00) : Color(0xFFBB0000)),
                )
              ])),
          SizedBox(
            child: Text(
              '----->',
            ),
            height: 40,
          ),
          SizedBox(
            width: 120,
            child: Column(children: <Widget>[
              SizedBox(
                height: 60.0,
                width: 80,
                child: Image.asset(
                  'assets/vk-logo.png',
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'Аккаунт ВК',
                style: TextStyle(color: widget.mainKey.currentState.vkAuth ? Color(0xFF00BB00) : Color(0xFFBB0000)),
                textAlign: TextAlign.center,
              )
            ]),
          )
        ],
      ),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Text(
                'Версия ${widget.mainKey.currentState.ver}',
                textAlign: TextAlign.right,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 10),
              logoText,
              SizedBox(height: MediaQuery.of(context).size.height / 12),
              widget.mainKey.currentState.vkNeedAuth ? Text('') : logo,
              SizedBox(height: MediaQuery.of(context).size.height / 12),
              forgotLabel,
              SizedBox(height: MediaQuery.of(context).size.height / 12),
              authIcons,
              SizedBox(height: MediaQuery.of(context).size.height / 11),
              loginButton,
              widget.mainKey.currentState.vkNeedAuth ? infoLabel : SizedBox(),
              widget.mainKey.currentState.alertmessage.length > 0 ? Text(widget.mainKey.currentState.alertmessage,textAlign: TextAlign.center) : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
