import 'package:flutter/material.dart';
import 'api.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_billing/flutter_billing.dart';
import 'dart:convert';

class SubPage extends StatefulWidget {
  SubPage({Key key, this.mainKey}) : super(key: key);

  final mainKey;

  @override
  _SubPageState createState() => new _SubPageState();
}

class _SubPageState extends State<SubPage> {
  final _formKey = GlobalKey<FormState>();
  String promo;
  var promoApproved = false;
  var promoTry = false;
  @override
  Widget build(BuildContext context) {
    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
              style: BorderStyle.solid,
            )),
        onPressed: (() async {
          widget.mainKey.currentState.setState(() {
            widget.mainKey.currentState.showSub = false;
          });
        }),
        padding: EdgeInsets.all(12),
        color: Colors.white,
        child: Text('Назад', style: TextStyle(color: Colors.black)),
      ),
    );

    final buyRow = Padding(
        padding: EdgeInsets.symmetric(horizontal: 4.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Card(
                child: Column(children: <Widget>[
              Text('Подписка на 6 месяцев', textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(
                      style: BorderStyle.solid,
                    )),
                onPressed: (() async {
                  _formKey.currentState.save();
                  if (promo.length > 0) {
                    FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).once().then((val) {
                      setState(() {
                        promoTry = true;
                        promoApproved = false;
                        if (val.value != null && val.value['enabled']) {
                          promoApproved = true;
                        }
                      });
                    });
                  }
                  await widget.mainKey.currentState.billing.purchase(promoApproved ? '0011' : '0001');
                  final Billing billing = new Billing(onError: (e) {});
                  var file = await getFile();
                  final prefs = jsonDecode(file.readAsStringSync());
                  var p01 = await billing.isPurchased('0001');
                  var p11 = await billing.isPurchased('0011');
                    if (p11 && promo.length > 0) {
                      FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).once().then((val) async {
                        if (val.value != null && val.value['enabled']) {
                          var file3 = await getFile3();
                          final prefs3 = jsonDecode(file.readAsStringSync());
                          prefs3['promo'] = promo.toString().toLowerCase();
                          await saveFile(file3, prefs3);
                          Map item = new Map();
                          item['timer'] = new DateTime.now().millisecondsSinceEpoch;
                          item['vk_username'] = prefs['vk_username'];
                          item['vk_id'] = prefs['vk_id'];
                          item['insta_username'] = widget.mainKey.currentState.insta_username;
                          item['date'] = DateTime.now().toString();
                          item['tarif'] = '6 месяцев';
                          item['disabled'] = false;
                          FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).child(prefs['vk_id']).set(item);
                        }
                      });
                    }
                    ;
                    widget.mainKey.currentState.setState(() {
                      widget.mainKey.currentState.p6 = p01 || p11;
                      widget.mainKey.currentState.getCount(widget.mainKey, null);
                    });
                }),
                padding: EdgeInsets.all(12),
                color: Colors.white,
                disabledColor: Colors.white70,
                child: promoApproved? Column(children: <Widget>[Text('890 рублей',style: TextStyle(decoration: TextDecoration.lineThrough,),), Text('690 рублей', style: TextStyle(color: Colors.red, ),)],) : Text('890 рублей'),
              ),
              SizedBox(height: 5,)
            ])),
            SizedBox(height: MediaQuery.of(context).size.height / 34),
            Card(
                child: Column(children: <Widget>[
              Text('Подписка на 12 месяцев', textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(
                      style: BorderStyle.solid,
                    )),
                onPressed: (() async {
                  _formKey.currentState.save();
                  if (promo.length > 0) {
                    FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).once().then((val) {
                      setState(() {
                        promoTry = true;
                        promoApproved = false;
                        if (val.value != null && val.value['enabled']) {
                          promoApproved = true;
                        }
                      });
                    });
                  }
                  await widget.mainKey.currentState.billing.purchase(promoApproved ? '0022' : '0002');

                  final Billing billing = new Billing(onError: (e) {});
                  var file = await getFile();
                  final prefs = jsonDecode(file.readAsStringSync());

                  var p02 = await billing.isPurchased('0002');
                  var p22 = await billing.isPurchased('0022');
                    if (p22 && promo.length > 0) {

                      FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).once().then((val) async {
                        if (val.value != null && val.value['enabled']) {
                          var file3 = await getFile3();
                          final prefs3 = jsonDecode(file.readAsStringSync());
                          prefs3['promo'] = promo.toString().toLowerCase();
                          await saveFile(file3, prefs3);
                          Map item = new Map();
                          item['timer'] = new DateTime.now().millisecondsSinceEpoch;
                          item['vk_username'] = prefs['vk_username'];
                          item['vk_id'] = prefs['vk_id'];
                          item['insta_username'] = widget.mainKey.currentState.insta_username;
                          item['date'] = DateTime.now().toString();
                          item['tarif'] = '12 месяцев';
                          item['disabled'] = false;

                          FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).child(prefs['vk_id']).set(item);
                        }
                      });
                    }
                    widget.mainKey.currentState.setState(() {
                      widget.mainKey.currentState.p12 = p02 || p22;
                      widget.mainKey.currentState.getCount(widget.mainKey, null);
                    });
                }),
                padding: EdgeInsets.all(12),
                color: Colors.white,
                disabledColor: Colors.white70,
                child: promoApproved? Column(children: <Widget>[Text('1290 рублей',style: TextStyle(decoration: TextDecoration.lineThrough,),), Text('990 рублей', style: TextStyle(color: Colors.red, ),)],) : Text('1290 рублей'),
              ),
                  SizedBox(height: 5,)
            ])),
          ],
        ));

    final yourSub = Padding(
        padding: EdgeInsets.symmetric(horizontal: 4.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('Ваша подписка:' + (widget.mainKey.currentState.p6 ? ' 6 месяцев' : ' 12 месяцев')),
          ],
        ));

    final email = Row(children: <Widget>[
      new Expanded(
          child: TextFormField(
        keyboardType: TextInputType.text,
        autofocus: false,
        onSaved: (String value) {
          promo = value;
        },
        decoration: InputDecoration(
          hintText: 'Промокод:',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(),
        ),
      )),
      SizedBox(
        width: 10,
      ),
      RaisedButton(
        color: Colors.green[300],
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        padding: EdgeInsets.symmetric(horizontal: 7, vertical: 11),
        onPressed: (() {
          _formKey.currentState.save();
          if (promo.length > 0) {
            FirebaseDatabase.instance.reference().child('promokeys').child(promo.toString().toLowerCase()).once().then((val) {
              promoTry = true;
              setState(() {
                promoApproved = false;
                if (val.value != null && val.value['enabled']) {
                  promoApproved = true;
                }
              });
            });
          }
        }),
        child: Text('Проверить'),
      )
    ]);

    return new Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Text(
                'Версия ${widget.mainKey.currentState.ver}',
                textAlign: TextAlign.center,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 40),
              widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12
                  ? Text(
                      'Подписка оформлена',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20),
                    )
                  : Text('Подписка не оформлена', textAlign: TextAlign.center, style: TextStyle(fontSize: 20)),
              SizedBox(
                height: 5,
              ),
              widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12
                  ? Text('')
                  : Text(
                      'Активация подписки даёт 3 дня бесплатного пользования. Вы сможете отменить подписку до списания денег, если вас не устроит работа программы.',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12)),
              widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12 ? SizedBox() : SizedBox(height: MediaQuery.of(context).size.height / 24),
              widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12 ? SizedBox() : email,
              promoTry ? promoApproved ? Text('Промокод подтвержден') : Text('Неверный промокод') : SizedBox(),
              widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12 ? SizedBox() : SizedBox(height: MediaQuery.of(context).size.height / 24),
              !widget.mainKey.currentState.p6 && !widget.mainKey.currentState.p12 ? buyRow : yourSub,
              loginButton,
            ],
          ),
        ),
      ),
    );
  }
}
