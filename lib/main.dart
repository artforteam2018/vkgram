import 'package:flutter/material.dart';
import 'InstaLoginPage.dart';
import 'landingPage/landingPage.dart';
import 'homepage.dart';
import 'subPage.dart';
import 'dart:async';
import 'api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'dart:isolate';
import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_billing/flutter_billing.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

void printHello() async {
  var maxTest = 8;
  var flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid = new AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = new IOSInitializationSettings(onDidReceiveLocalNotification: null);
  var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);

  FirebaseDatabase.instance.reference().child('notifymessage').once().then((val) async {
    if (val.value != null && val.value.length > 0) {
      var file3 = await getFile3();
      final prefs = jsonDecode(file3.readAsStringSync());

      if (prefs['notifymessage'] == null || val.value != prefs['notifymessage']) {
        prefs['notifymessage'] = val.value;
        await saveFile(file3, prefs);
        flutterLocalNotificationsPlugin.initialize(initializationSettings);
        var androidPlatformChannelSpecifics =
        new AndroidNotificationDetails('vkgram', 'autopublish', 'autopublishing stories', importance: Importance.Max, priority: Priority.High);
        var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
        var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
        flutterLocalNotificationsPlugin.show(0, 'Важное сообщение', val.value, platformChannelSpecifics, payload: 'item x');
      }
    }
  });

  var file = await getFile();

  final prefs = jsonDecode(file.readAsStringSync());
  var auto = prefs['auto'];
  var taken = prefs['lastPublish'];
  if (auto == null) {
    auto = false;
  }
  if (auto) {
    var storiesResponse = await getStoriesIsolate();

    if (storiesResponse != 'no stories') {
      var fullUnpublish = storiesResponse.where((s) => taken == null || s['taken_at'] > int.parse(taken) == true).toList();

      if (fullUnpublish.length <= 0) return;
      var success = await publishStoryIsolate(fullUnpublish, maxTest);
      var answer = '';
      switch (success) {
        case 0:
          if (prefs['demo'] == null) {
            prefs['demo'] = 1;
            await saveFile(file, prefs);
            answer = 'Демо публикации закончились';
          } else {
            return;
          }
          break;
        case 1:
          answer = 'Опубликована новая история';
          break;
        case -1:
          return;
          break;
      }
      flutterLocalNotificationsPlugin.initialize(initializationSettings);
      var androidPlatformChannelSpecifics =
      new AndroidNotificationDetails('vkgram', 'autopublish', 'autopublishing stories', importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      flutterLocalNotificationsPlugin.show(0, 'Автопубликация', answer, platformChannelSpecifics, payload: 'item x');
    }
  }
}

printHello2(timer) async {
  var maxTest = 8;
  var flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid = new AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = new IOSInitializationSettings(onDidReceiveLocalNotification: null);
  var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
  var file = await getFile();

  final prefs = jsonDecode(file.readAsStringSync());
  var auto = prefs['auto'];
  var taken = prefs['lastPublish'];

  FirebaseDatabase.instance.reference().child('notifymessage').once().then((val) async {
    if (val.value != null && val.value.length > 0) {
      var file3 = await getFile3();
      final prefs = jsonDecode(file3.readAsStringSync());

      if (prefs['notifymessage'] == null || val.value != prefs['notifymessage']) {
        prefs['notifymessage'] = val.value;
        await saveFile(file3, prefs);
        flutterLocalNotificationsPlugin.initialize(initializationSettings);
        var androidPlatformChannelSpecifics =
        new AndroidNotificationDetails('vkgram', 'autopublish', 'autopublishing stories', importance: Importance.Max, priority: Priority.High);
        var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
        var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
        flutterLocalNotificationsPlugin.show(0, 'Важное сообщение', val.value, platformChannelSpecifics, payload: 'item x');
      }
    }
  });

  if (auto == null) {
    auto = false;
  }
  if (auto) {
    var storiesResponse = await getStoriesIsolate();

    if (storiesResponse != 'no stories') {
      var fullUnpublish = storiesResponse.where((s) => taken == null || s['taken_at'] > int.parse(taken) == true).toList();

      if (fullUnpublish.length <= 0) return;
      var success = await publishStoryIsolate(fullUnpublish, maxTest);
      var answer = '';
      switch (success) {
        case 0:
          if (prefs['demo'] == null) {
            prefs['demo'] = 1;
            await saveFile(file, prefs);
            answer = 'Демо публикации закончились';
          } else {
            return;
          }
          break;
        case 1:
          answer = 'Опубликована новая история';
          break;
        case -1:
          return;
          break;
      }
      flutterLocalNotificationsPlugin.initialize(initializationSettings);
      var androidPlatformChannelSpecifics =
      new AndroidNotificationDetails('vkgram', 'autopublish', 'autopublishing stories', importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      flutterLocalNotificationsPlugin.show(0, 'Автопубликация', answer, platformChannelSpecifics, payload: 'item x');
    }
  }
}

main() async {
  final int helloAlarmID = 0;
  await AndroidAlarmManager.initialize();
  runApp(MyApp());
  await AndroidAlarmManager.periodic(const Duration(seconds: 5), helloAlarmID, printHello, wakeup: true, rescheduleOnReboot: true);
}

class MainStateWidget extends StatefulWidget {
  static final mainKey = new GlobalKey<MainState>();

  MainStateWidget({Key key}) : super(key: mainKey);

  @override
  MainState createState() => new MainState();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainStateWidget(),
    );
  }
}

class MainState extends State<MainStateWidget> {
  LoginData _data = new LoginData();
  var needLogin = false;
  Timer timer;
  var platform = MethodChannel('crossingthestreams.io/resourceResolver');
  final ver = '1.4.2';
  final maxTest = 8;
  final vkUrl =
      'https://oauth.vk.com/authorize?client_id=6858524&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.92&revoke=1&scope=+65600';
  GlobalKey mam = new GlobalKey();
  var vkAuth = false;
  var instaAuth = false;
  var vkNeedAuth = false;
  var instaNeedAuth = false;
  var loaded = false;
  var noConnection = false;
  var showSub = false;
  var oldVkUrl = '';
  String insta_username = '';
  String vk_username = '';
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  final Billing billing = new Billing(onError: (e) {});
  var p6 = false;
  var p12 = false;
  var vkToken = '';
  var pNeed = false;
  var testP = 0;
  var promo = '';
  var alertmessage = '';
  var test = '';

  setLoginData(email, password) {
    if (email != null) _data.email = email;
    if (password != null) _data.password = password;
  }

  getLoginData() {
    return _data;
  }

  getVkToken(key, prefs) async {
    key.currentState.vkAuth = prefs['token'] == null ? false : true;
    return prefs['token'];
  }

  getCount(key, prefs) async {
    if (prefs == null) {
      var file = await getFile();
      prefs = jsonDecode(file.readAsStringSync());
    }
    key.currentState.setState(() {
      key.currentState.testP = prefs['testP'];
      if (key.currentState.testP >= maxTest && !key.currentState.p6 && !key.currentState.p12) {
        key.currentState.pNeed = true;
      } else {
        key.currentState.pNeed = false;
      }
    });

    return prefs['testP'];
  }

  setVkToken(token, vkId) async {
    var file = await getFile();
    final prefs = jsonDecode(file.readAsStringSync());
    prefs['token'] = token;
    prefs['vk_id'] = vkId;
    if (token == null && vkId == null) {
      prefs['vk_username'] = null;
      vk_username = null;
    } else {
      var username = await vk_setUsername(prefs);
      prefs['vk_username'] = username['first_name'];
      await saveFile(file, prefs);
      vk_username = username['first_name'] + ' ' + username['last_name'];
    }
  }

  @override
  initState() {
    super.initState();
    try {
      getFile().then((file) async {
        var p01 = await billing.isPurchased('0001');
        var p11 = await billing.isPurchased('0011');
        var p02 = await billing.isPurchased('0002');
        var p22 = await billing.isPurchased('0022');
        FirebaseDatabase.instance.reference().child('debug').push().set(p01.toString() + p11.toString() + p02.toString() + p22.toString());
        final prefs = jsonDecode(file.readAsStringSync());
        if (p01 || p11) {
          prefs['p6'] = true;
          await saveFile(file, prefs);
          setState(() {
            p6 = true;
          });
        } else {
          prefs['p6'] = false;
          await saveFile(file, prefs);
          var file3 = await getFile3();
          final prefs3 = jsonDecode(file3.readAsStringSync());
          if (prefs3['promo'] != null && prefs3['vk_id'] != null) {
            var timer = await FirebaseDatabase.instance.reference().child('promokeys').child(prefs3['promo']).child(prefs['vk_id']).once();
            if (timer.value != null) {
              var timerV = timer.value['timer'];
              if (prefs['p6'] == true && timer != null && new DateTime.now().millisecondsSinceEpoch - timerV < 86400000 * 3) {
                FirebaseDatabase.instance.reference().child('promokeys').child(prefs3['promo']).child(prefs['vk_id']).child('disabled').set(true);
              }
            }
          }
        }
        if (p02 || p22) {
          prefs['p12'] = true;
          await saveFile(file, prefs);
          setState(() {
            p12 = true;
          });
        } else {
          prefs['p12'] = false;
          getCount(widget.key, prefs);
          await saveFile(file, prefs);
          var file3 = await getFile3();
          final prefs3 = jsonDecode(file3.readAsStringSync());
          if (prefs3['promo'] != null && prefs3['vk_id'] != null) {
            var timer = await FirebaseDatabase.instance.reference().child('promokeys').child(prefs3['promo']).child(prefs['vk_id']).once();
            if (timer.value != null) {
              var timerV = timer.value['timer'];
              if (prefs['p12'] == true && timer != null && new DateTime.now().millisecondsSinceEpoch - timerV < 86400000 * 3) {
                FirebaseDatabase.instance.reference().child('promokeys').child(prefs3['promo']).child(prefs['vk_id']).child('disabled').set(true);
              }
            }
          }
        }
      });

      FirebaseDatabase.instance.reference().child('alertmessage').once().then((val) {
        if (val.value != null && val.value.length > 0) {
          setState(() {
            alertmessage = val.value;
          });
        }
      });

      new Timer.periodic(const Duration(minutes: 1), printHello2);
      Connectivity().checkConnectivity().then((result) async {
        if (result != ConnectivityResult.none) {
          new Timer(Duration(seconds: 1), () async {
            var file = await getFile();
            final prefs = jsonDecode(file.readAsStringSync());
            getCookies(widget.key, prefs).then((res) {
              getVkToken(widget.key, prefs).then((res) {
                vk_setUsername(prefs).then((result) {
                  setState(() {
                    if (result != null) vk_username = result['first_name'] + ' ' + result['last_name'];
                    loaded = true;
                  });
                  insta_setUsername(prefs).then((result) =>
                      setState(() {
                        insta_username = result;
                      }));
                });
              });
            });
          });
        } else {
          setState(() {
            checkConnection(widget.key);

            noConnection = true;
          });
        }
      });
    } catch (e) {
      test = 's' + jsonEncode(e);
      FirebaseDatabase.instance.reference().child('errors').push().set(jsonEncode(e));
    }
  }

  @override
  Widget build(BuildContext context) {
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      RegExp regExp = new RegExp('(access_token=)(.*)(&expires_in)');
      RegExp regExp2 = new RegExp('(user_id=)(.*)');
      var resultMatch = regExp.allMatches(url).toList();
      var resultMatch2 = regExp2.allMatches(url).toList();
      if (resultMatch.length > 0) {
        if (vkToken != resultMatch[0].group(2)) {
          vkToken = resultMatch[0].group(2);
          var vkId = resultMatch2[0].group(2);
          setState(() {
            vkNeedAuth = false;
            vkAuth = true;
            setVkToken(resultMatch[0].group(2), vkId);
            flutterWebviewPlugin.close();
          });
        }
      }
    });

    return MaterialApp(
        title: 'App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
          fontFamily: 'IBMPlex',
        ),
        home: mainPage(widget));
  }

  Widget mainPage(widget) {
    if (noConnection) {
      return Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Text(
                'Нет соединения с интернетом',
                textAlign: TextAlign.center,
              )
            ]),
          ));
    }
    if (showSub) {
      return SubPage(mainKey: widget.key);
    }
    if (widget.key.currentState.instaAuth && widget.key.currentState.vkAuth) {
      return HomePage(mainKey: widget.key);
    }
    if (widget.key.currentState.instaNeedAuth) {
      return InstaLoginPage(mainKey: widget.key);
    } else if (loaded) {
      return LandingPage(mainKey: widget.key);
    } else {
      return Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Text(test)
            ]),
          ));
    }
  }
}

class LoginData {
  String email = '';
  String password = '';
}
