import 'package:flutter/material.dart';
import 'api.dart';
import 'dart:async';
import 'storyGetter/storyGetter.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

class HomePage extends StatefulWidget {
  static final homeKey = new GlobalKey<_HomePageState>();

  HomePage({Key key, this.mainKey}) : super(key: homeKey);
  static String tag = 'login-page';
  final mainKey;

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static String tag = 'HomePage';

  var _item = false;

  var checkPublishingStr = '';
  var flutterUrl =
      'https://scontent-frx5-1.cdninstagram.com/vp/55b5b32e499a0ff8a13913b444401bda/5C68FFFF/t50.12441-16/52143384_1027798897423117_1032111956525800754_n.mp4?_nc_ht=scontent-frx5-1.cdninstagram.com';
  Timer timer;
  var flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  var expanded = false;
  var auto;
  var current = 0;

  dynamic stories = '';

  setLoaderIcon(item) {
    _item = item;
  }

  checkLoaderIcon() {
    return _item;
  }

  Future repeatNotification() async {
    timer = Timer.periodic(new Duration(seconds: 10), (timer) async {
      getStories(widget.key, true, widget.mainKey);
    });
  }

  @override
  initState() {
    super.initState();

    var initializationSettingsAndroid = new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings(onDidReceiveLocalNotification: onDidRecieveLocalNotification);
    var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    getStories(widget.key, true, widget.mainKey);
    updateAuto(widget.key);
    repeatNotification();
  }

  Future onDidRecieveLocalNotification(int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
            title: new Text(title),
            content: new Text(body),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final body = Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Colors.white,
            Colors.white70,
          ]),
        ),
        child: StoryGetter(widget, context));

    return Scaffold(
      body: body,
    );
  }
}

List<String> litems = ["1", "2", "Third", "4"];
List<Widget> lwid = litems.map((item) => new Text(item)).toList();
