import 'package:flutter/material.dart';
import '../api.dart';
import 'dart:convert';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:url_launcher/url_launcher.dart';

Widget storyViewer(widget, context) {
  final logo = Hero(
    tag: 'hero',
    child: CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: 35.0,
      child: Image.asset('assets/logo.png'),
    ),
  );

  final authIcons = Padding(
    padding: EdgeInsets.only(left: 10.0, right: 10.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          child: Row(children: <Widget>[
            SizedBox(
              height: 40.0,
              width: 40,
              child: Image.asset(
                'assets/Instagram.png',
                fit: BoxFit.fitWidth,
              ),
            ),
            SizedBox(width: 10),
            SizedBox(
              height: MediaQuery.of(context).size.height / 12,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height / 75),
                  SizedBox(
                      width: MediaQuery.of(context).size.width / 3.2,
                      child: Text(
                        widget.mainKey.currentState.insta_username,
                        textAlign: TextAlign.start,
                        style: TextStyle(fontSize: 12),
                        maxLines: 2,
                      )),
                  InkWell(
                      child: Text(
                        'Выйти',
                        textAlign: TextAlign.start,
                        style: TextStyle(fontSize: 12, color: Colors.indigo, decoration: TextDecoration.underline),
                      ),
                      onTap: (() {
                        widget.mainKey.currentState.setState(() {
                          clearCookies();
                          widget.mainKey.currentState.instaAuth = false;
                        });
                      }))
                ],
              ),
            )
          ]),
        ),
        SizedBox(width: MediaQuery.of(context).size.width / 100),
        SizedBox(
          child: Row(children: <Widget>[
            SizedBox(
              height: 40.0,
              width: 40,
              child: Image.asset(
                'assets/vk-logo.png',
                fit: BoxFit.fitWidth,
              ),
            ),
            SizedBox(width: 10),
            SizedBox(
              height: MediaQuery.of(context).size.height / 12,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height / 75),
                  SizedBox(
                      width: MediaQuery.of(context).size.width / 3.2,
                      child: widget.mainKey.currentState.vk_username == null
                          ? Image.asset('assets/loader.gif', height: 20)
                          : Text(
                              widget.mainKey.currentState.vk_username,
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 12),
                              maxLines: 2,
                            )),
                  InkWell(
                      child: Text(
                        'Выйти',
                        textAlign: TextAlign.start,
                        style: TextStyle(fontSize: 12, color: Colors.indigo, decoration: TextDecoration.underline),
                      ),
                      onTap: (() {
                        widget.mainKey.currentState.setState(() {
                          widget.mainKey.currentState.vkAuth = false;
                          widget.mainKey.currentState.flutterWebviewPlugin.cleanCookies();
                          widget.mainKey.currentState.setVkToken(null, null);
                        });
                      }))
                ],
              ),
            )
          ]),
        ),
      ],
    ),
  );

  final carousel = widget.key.currentState.stories is List
      ? Stack(children: [
          new CarouselSlider(
            initialPage: widget.key.currentState.stories.length - 1,
            items: new List<Widget>.from(widget.key.currentState.stories.map((story) {
              return new Builder(
                builder: (BuildContext context) {
                  return Container(
                      margin: EdgeInsets.all(5),
                      //width: MediaQuery.of(context).size.width * 0.9,
                      //height: MediaQuery.of(context).size.width * 0.9,
                      decoration: new BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                story['image_versions2']['candidates'][0]['url'],
                              ),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Align(
                              alignment: Alignment(-1, 0),
                              child: Icon(story['media_type'] == 1 ? Icons.image : Icons.video_library, color: Color(0xBBFFFFFF), size: 55)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[],
                          )
                        ],
                      ));
                },
              );
            })),
            distortion: false,
            height: MediaQuery.of(context).size.height / 3,
            viewportFraction: 1.0,
            updateCallback: (index) {
              widget.key.currentState.setState(() {
                widget.key.currentState.current = index;
              });
            },
          ),
          Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: new List<Widget>.from(
                    widget.key.currentState.stories.map((story) {
                      return Container(
                        width: 8.0,
                        height: 8.0,
                        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: widget.key.currentState.current == widget.key.currentState.stories.indexOf(story)
                                ? Color.fromRGBO(255, 255, 255, 0.9)
                                : Color.fromRGBO(255, 255, 255, 0.4)),
                      );
                    }),
                  )))
        ])
      : Text('');

  final ifCarousel = widget.key.currentState.stories == ''
      ? SizedBox(
          child: Image.asset(
            'assets/loader.gif',
          ),
          height: 60,
        )
      : widget.key.currentState.stories == 'no stories'
          ? Align(alignment: Alignment(0, 0), child: Text('Историй не найдено', textAlign: TextAlign.center))
          : carousel;

  final loginButton = Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
              style: BorderStyle.solid,
            )),
        onPressed: (() => publishStory(widget.key, widget.mainKey, widget.key.currentState.stories)),
        padding: EdgeInsets.all(12),
        color: Colors.white,
        disabledColor: Colors.white70,
        child: widget.key.currentState.checkLoaderIcon()
            ? Image.asset('assets/loader.gif', height: 20)
            : SizedBox(
                child: Text('Опубликовать', style: TextStyle(color: Colors.black)),
                height: 20,
              ),
      ));

  final noPublishButton = Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
              style: BorderStyle.solid,
            )),
        onPressed: (() => unPublishStory(widget.key, widget.mainKey, widget.key.currentState.stories)),
        padding: EdgeInsets.all(12),
        color: Colors.white,
        disabledColor: Colors.white70,
        child: SizedBox(
          child: Text('Не публиковать загруженные', style: TextStyle(color: Colors.black)),
          height: 20,
        ),
      ));

  final buyRow = Padding(
      padding: EdgeInsets.symmetric(horizontal: 4.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Количество бесплатных публикаций ограничено. Необходимо приобрести подписку',
            textAlign: TextAlign.center,
          ),
          RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
                side: BorderSide(
                  style: BorderStyle.solid,
                )),
            padding: EdgeInsets.all(12),
            color: Colors.white,
            disabledColor: Colors.white70,
            child: Text('Подписка'),
            onPressed: (() {
              widget.mainKey.currentState.setState(() {
                widget.mainKey.currentState.showSub = true;
              });
            }),
          )
        ],
      ));

  final handButton = FlatButton(
    onPressed: (() async {
      var file = await getFile();
      final prefs = jsonDecode(file.readAsStringSync());
      prefs['auto'] = false;
      await saveFile(file, prefs);

      widget.key.currentState.setState(() {
        widget.key.currentState.auto = false;
        widget.key.currentState.expanded = false;
      });
    }),
    color: Colors.white,
    disabledColor: Colors.white70,
    child: SizedBox(
      width: MediaQuery.of(context).size.width * 0.9,
      child: Text('Ручная публикация'),
    ),
  );

  final autoButton = FlatButton(
    onPressed: (() async {
      var file = await getFile();
      final prefs = jsonDecode(file.readAsStringSync());
      prefs['auto'] = true;
      await saveFile(file, prefs);

      var last = prefs['lastPublish'];
      dynamic publishStories = new List();
      if (widget.key.currentState.stories != 'no stories') {
        if (last != null) {
          var lastInt = int.parse(last);
          publishStories = widget.key.currentState.stories.where((story) => story['taken_at'] > lastInt == true).toList();
        } else {
          publishStories = widget.key.currentState.stories;
        }
      }
      widget.key.currentState.setState(() {
        widget.key.currentState.auto = true;
        widget.key.currentState.expanded = false;
        publishStory(widget.key, widget.mainKey, publishStories);
      });
    }),
    color: Colors.white,
    disabledColor: Colors.white70,
    child: SizedBox(
      width: MediaQuery.of(context).size.width * 0.9,
      child: Text('Авто публикация'),
    ),
  );

  final infoPublish = Text(
    widget.key.currentState.stories is List ? widget.key.currentState.checkPublishingStr : '',
    style: TextStyle(color: Colors.black54, fontSize: 14),
    textAlign: TextAlign.center,
  );

  final pLeft = Text(
    'Осталось ' +
        (widget.mainKey.currentState.maxTest - (widget.mainKey.currentState.testP == null ? 0 : widget.mainKey.currentState.testP) < 0
                ? 0
                : widget.mainKey.currentState.maxTest - (widget.mainKey.currentState.testP == null ? 0 : widget.mainKey.currentState.testP))
            .toString() +
        ' публикаций.',
    style: TextStyle(color: Colors.black54, fontSize: 14),
    textAlign: TextAlign.center,
  );

  final infoPanel = Text(
    'Последняя история',
    style: TextStyle(color: Colors.black54, fontSize: 24),
    textAlign: TextAlign.center,
  );

  final selector = Padding(
    padding: EdgeInsets.symmetric(horizontal: 5),
    child: ExpansionPanelList(
      expansionCallback: ((ff, bb) => widget.key.currentState.setState(() {
            widget.key.currentState.expanded = !widget.key.currentState.expanded;
          })),
      children: <ExpansionPanel>[
        new ExpansionPanel(
            headerBuilder: (BuildContext context, bool isExpanded) => Align(
                alignment: Alignment(0, 0),
                child: widget.key.currentState.auto == null
                    ? Image.asset('assets/loader.gif', height: 20)
                    : widget.key.currentState.auto ? Text("Авто публикация") : Text('Ручная публикация')),
            body: SizedBox(
                height: MediaQuery.of(context).size.height / 9,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 40,
                      child: autoButton,
                    ),
                    Container(height: 40, child: handButton)
                  ],
                )),
            isExpanded: widget.key.currentState.expanded),
      ],
    ),
  );

  return new ListView(children: <Widget>[
    Padding(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FlatButton(
              padding: EdgeInsets.all(0),
              color: Colors.white,
              disabledColor: Colors.white70,
              child: Row(
                children: <Widget>[
                  Text('Наша группа ВК'),
                  SizedBox(
                    width: 10,
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 15.0,
                    child: Image.asset('assets/logo.png'),
                  ),
                ],
              ),
              onPressed: (() => launch('https://vk.com/vkgramstories')),
            ),
            FlatButton(
              child: Text('Подписка'),
              onPressed: (() {
                widget.mainKey.currentState.setState(() {
                  widget.mainKey.currentState.showSub = true;
                });
              }),
            )
          ],
        )),
    SizedBox(height: MediaQuery.of(context).size.height / 45),
    Text(
      'Режим публикаций',
      textAlign: TextAlign.center,
    ),
    SizedBox(height: MediaQuery.of(context).size.height / 65),
    selector,
    authIcons,
    infoPanel,
    SizedBox(height: MediaQuery.of(context).size.height / 60),
    ifCarousel,
    SizedBox(height: MediaQuery.of(context).size.height / 40),
    infoPublish,
    widget.mainKey.currentState.p6 || widget.mainKey.currentState.p12 ? SizedBox() : pLeft,
    SizedBox(height: MediaQuery.of(context).size.height / 60),
    widget.key.currentState.stories is List ? widget.mainKey.currentState.pNeed ? buyRow : loginButton : SizedBox(),
    widget.key.currentState.auto == null || widget.key.currentState.auto ? SizedBox() : noPublishButton,
    widget.mainKey.currentState.alertmessage.length > 0 ? Text(widget.mainKey.currentState.alertmessage,textAlign: TextAlign.center,) : SizedBox(),
    SizedBox(height: MediaQuery.of(context).size.height / 30),
  ]);
}
