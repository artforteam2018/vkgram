import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';
import 'utils.dart';
import 'package:device_info/device_info.dart';
import 'package:intl/intl.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter_billing/flutter_billing.dart';

const BASE_URL = 'https://i.instagram.com/api/v1/si/fetch_headers/';
const LOGIN_URL = 'https://i.instagram.com/api/v1/accounts/login/';
const SIGNATURE = '99e16edcca71d7c1f3fd74d447f6281bd5253a623000a55ed0b60014467a53b1';
const KEY_VERSION = '4';

VK_GETUSER_URL(token) => 'https://api.vk.com/method/account.getProfileInfo?access_token=$token&v=5.92';

VK_UPLOADPHOTO_URL(token) => 'https://api.vk.com/method/stories.getPhotoUploadServer?add_to_news=1&access_token=$token&v=5.92';

VK_UPLOADVIDEO_URL(token) => 'https://api.vk.com/method/stories.getVideoUploadServer?add_to_news=1&access_token=$token&v=5.92';

STORIES_URL(id) => 'https://i.instagram.com/api/v1/feed/user/$id/story/';

getDefaultHeaders() async {
  Options options = new Options();
  Map<String, dynamic> headers = new Map();
  headers['User-Agent'] = await getUserAgent();
  headers['Connection'] = 'close';
  headers['Accept'] = '*/*';
  headers['Accept-Language'] = 'en-US';
  headers['Accept-Encoding'] = 'gzip, deflate';
  headers['X-IG-Capabilities'] = '3brTBw==';
  headers['X-IG-Connection-Type'] = 'WIFI';
  headers['X-IG-Connection-Speed'] = '1730kbps';
  headers['X-IG-App-ID'] = '567067343352427';
  headers['X-IG-Bandwidth-Speed-KBPS'] = '-1.000';
  headers['X-IG-Bandwidth-TotalBytes-B'] = '0';
  headers['X-IG-Bandwidth-TotalTime-MS'] = '0';
  headers['X-FB-HTTP-Engine'] = 'Liger';
  headers['Content-type'] = 'multipart/form-data';
  options.contentType = ContentType.parse('multipart/form-data');
  options.headers = headers;
  return options;
}

getUserAgent() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
  return 'Instagram 26.0.0.10.86 Android (24/7.0; 640dpi; 1440x2560; ${androidInfo.brand}; ${androidInfo.model}; herolte; ${androidInfo.hardware}}; en_US)';
}

log(e) async {
  Map error = new Map();
  error['time'] = DateTime.now().toString();

  if (e is DioError) {
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");

    error['cookie'] = cookie.loadForRequest(Uri.parse(e.response.request.path)).toList().toString();
    if (e.response != null) {
      error['response_data'] = e.response.data.toString();
      error['response_headers'] = e.response.headers.toString();
      error['request_headers'] = e.response.request.headers.toString();
      error['request_data'] = e.response.request.data.toString();
    } else {
      // Something happened in setting up or sending the request that triggered an Error
      error['request_headers'] = e.response.request.headers.toString();
      error['request_data'] = e.response.request.data.toString();
    }
    print(error.toString());
    final notesReference = FirebaseDatabase.instance.reference().child('log');
    notesReference.push().set(error).then((_) {
      // ...
    });
  }
}

//['image_versions2']['candidates'][0]['url']
uploadStory(token, story) async {
  var file = await getFile2();
  final prefs = jsonDecode(file.readAsStringSync());
  if (prefs[story['taken_at'].toString()] != null) {
    return 'История уже опубликована';
  }
  else {
    prefs[story['taken_at'].toString()] = true;
    await saveFile(file, prefs);
  }

  if (token == null) return 'Аккаунт Вконтакте не подключен';
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();

    if (story['media_type'] == 1) {
      var image = await dio.download(story['image_versions2']['candidates'][0]['url'], appDocDirectory.path + "/temp.jpg");
      var response = await dio.get(VK_UPLOADPHOTO_URL(token));
      var path = response.data['response']['upload_url'];
      var file = new File(appDocDirectory.path + "/temp.jpg");

      try {
        response = await dio.post(path, data: new FormData.from({'file': new UploadFileInfo(file, appDocDirectory.path + "/temp.jpg")}));
      } catch (e) {
        log(e);
        return 'Внутренняя ошибка';
      }
      if (response.data['response'] != null) {
        return 'Фотография загружена';
      } else {
        return response.data['error']['type'];
      }
    } else {
      var video = await dio.download(story['video_versions'][0]['url'], appDocDirectory.path + "/temp.mp4");
      var response = await dio.get(VK_UPLOADVIDEO_URL(token));
      var path = response.data['response']['upload_url'];

      var file = new File(appDocDirectory.path + "/temp.mp4");

      try {
        response = await dio.post(path, data: new FormData.from({'video_file': new UploadFileInfo(file, appDocDirectory.path + "/temp.mp4")}));
      } catch (e) {
        log(e);
        return 'Внутренняя ошибка';
      }
      if (response.data['response'] != null) {
        return 'Видео загружено';
      } else {
        return response.data['error']['type'];
      }
    }
}

login(data, mainKey, showAlert) async {
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  dio.interceptors.add(CookieManager(cookie));
  var response;
  Map oldLogin = new Map();
  try {
    response = await dio.get(BASE_URL, options: await getDefaultHeaders());
  } catch (e) {
    log(e);
    return 'Внутренняя ошибка';
  }

  if (response.statusCode == 200) {
    var deviceId = 'android-' + generate_uuid(16, true);
    var uuid = generate_uuid(36, false);
    var adid = generate_adid(36, false, data.email);
    var phoneId = generate_adid(36, false, deviceId);
    oldLogin['deviceId'] = deviceId;
    oldLogin['uuid'] = uuid;
    oldLogin['adid'] = adid;
    oldLogin['phoneId'] = phoneId;
    var loginData = new FormData.from({
      'device_id': deviceId,
      'guid': uuid,
      'adid': adid,
      'phone_id': phoneId,
      'csrftoken': cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'csrftoken').toList()[0].value,
      'username': data.email,
      'password': data.password,
      'login_attempt_count': '0',
    });
    var encodedLoginData = jsonEncode(loginData);
    var Hmaclogin = getHmac(SIGNATURE, encodedLoginData).toString();
    loginData['signed_body'] = Hmaclogin;
    loginData['ig_sig_key_version'] = KEY_VERSION;
    try {
      response = await dio.post(LOGIN_URL, data: loginData, options: await getDefaultHeaders());

      var file = await getFile();
      final prefs = jsonDecode(file.readAsStringSync());

      try {
        prefs['insta_username'] = response.data['logged_in_user']['username'];
        mainKey.currentState.insta_username = response.data['logged_in_user']['username'];
      } catch (e) {
        prefs['insta_username'] = 'Аккаунт Instagram';
        mainKey.currentState.insta_username = 'Аккаунт Instagram';
      }

      prefs['instaAuth'] = true;

      await saveFile(file, prefs);

      return 'Успешно';
    } catch (e) {
      if (e.response.data['message'] == 'challenge_required') {
        var path = 'https://i.instagram.com/api/v1' + e.response.data['challenge']['api_path'];
        showAlert(
            path,
            new FormData.from({'_csrftoken': cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'csrftoken').toList()[0].value}),
            data,
            mainKey,
            showAlert,
            oldLogin);
        return 'Требуется подтверждение';
      } else {
        log(e);
        return 'Неправильный логин или пароль';
      }
    }
  }
}

loginAgain(data, mainKey, showAlert, oldLogin) async {
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  dio.interceptors.add(CookieManager(cookie));
  var response;

  var deviceId = oldLogin['deviceId'];
  var uuid = oldLogin['uuid'];
  var adid = oldLogin['adid'];
  var phoneId = oldLogin['phoneId'];
  var loginData = new FormData.from({
    'device_id': deviceId,
    'guid': uuid,
    'adid': adid,
    'phone_id': phoneId,
    'csrftoken': cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'csrftoken').toList()[0].value,
    'username': data.email,
    'password': data.password,
    'login_attempt_count': '0',
  });
  var encodedLoginData = jsonEncode(loginData);
  var Hmaclogin = getHmac(SIGNATURE, encodedLoginData).toString();
  loginData['signed_body'] = Hmaclogin;
  loginData['ig_sig_key_version'] = KEY_VERSION;
  try {
    response = await dio.post(LOGIN_URL, data: loginData, options: await getDefaultHeaders());

    var file = await getFile();
    final prefs = jsonDecode(file.readAsStringSync());

    try {
      prefs['insta_username'] = response.data['logged_in_user']['username'];
      mainKey.currentState.insta_username = response.data['logged_in_user']['username'];
    } catch (e) {
      prefs['insta_username'] = 'Аккаунт Instagram';
      mainKey.currentState.insta_username = 'Аккаунт Instagram';
    }
    prefs['instaAuth'] = true;
    await saveFile(file, prefs);
    return 'Успешно';
  } catch (e) {
    if (e.response.data['message'] == 'challenge_required') {
      var path = 'https://i.instagram.com/api/v1' + e.response.data['challenge']['api_path'];
      showAlert(
          path, new FormData.from({'_csrftoken': cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'csrftoken').toList()[0].value}));
    } else {
      log(e);
      return 'Неправильный логин или пароль';
    }
  }
}

challenge(path, formData) async {
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  dio.interceptors.add(CookieManager(cookie));
  try {
    var response = await dio.post(path, data: formData);
    return true;
  } catch (e) {
    log(e);
    return false;
  }
}

getStories(key, store, mainKey) async {
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  dio.interceptors.add(CookieManager(cookie));
  var response;

  response = await storiesRequest(cookie, dio, key, mainKey);
  var stories = await returnStories(response);
  if (store) {
    try {
      key.currentState.setState(() {
        key.currentState.stories = stories;
        key.currentState.current = stories.length - 1;
      });
    } catch (e) {}
  }
  checkPublishing(key, stories);
  return stories;
}

getStoriesIsolate() async {
  Dio dio = new Dio();
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  dio.interceptors.add(CookieManager(cookie));
  var response;

  response = await storiesRequestIsolate(cookie, dio);
  var stories = await returnStories(response);

  return stories;
}

storiesRequestIsolate(cookie, dio) async {
  var cookies = cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'ds_user_id').toList();
  if (cookies != null) {
    var userId = cookies[0].value;

    try {
      return await dio.get(STORIES_URL(userId), options: await getDefaultHeaders());
    } catch (e) {
      log(e);
      return null;
    }
  } else {
    return null;
  }
}

storiesRequest(cookie, dio, key, mainKey) async {
  var cookies = cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'ds_user_id').toList();
  if (cookies != null) {
    var userId = cookies[0].value;

    try {
      return await dio.get(STORIES_URL(userId), options: await getDefaultHeaders());
    } catch (e) {
      mainKey.currentState.setState(() {
        Connectivity().checkConnectivity().then((result) {
          if (result != ConnectivityResult.none) {
            mainKey.currentState.instaAuth = false;
          } else {
            mainKey.currentState.noConnection = true;
            checkConnection(mainKey);
          }
        });
      });
      log(e);
      return null;
    }
  } else {
    return null;
  }
}

returnStories(response) {
  if (response != null && response.data['reel'] != null) {
    var requests = Future.wait(Iterable.castFrom(response.data['reel']['items'].map((image) async {
      return image;
    })));
    return requests;
  }
  return 'no stories';
}

getCookies(key, prefs) async {
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
  var userCookie = cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'ds_user_id').toList();

  key.currentState.setState(() {
    if (!key.currentState.instaAuth) key.currentState.instaAuth = userCookie.length > 0 ? true : false;
  });

  if (userCookie.length <= 0) {
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    CookieJar cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");
    var userCookie = cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'ds_user_id').toList();

    key.currentState.setState(() {
      if (!key.currentState.instaAuth) key.currentState.instaAuth = userCookie.length > 0 ? true : key.currentState.instaAuth;
    });

    key.currentState.setState(() {
      key.currentState.instaAuth = prefs['instaAuth'] == null ? key.currentState.instaAuth : prefs['instaAuth'];
    });

    return userCookie.length > 0 ? userCookie : null;
  } else {
    return userCookie;
  }
}

clearCookies() async {
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  var cookie = new PersistCookieJar(dir: appDocDirectory.path + "./cookies");

  cookie.delete(Uri.parse(BASE_URL), true);
  var userCookie = cookie.loadForRequest(Uri.parse(BASE_URL)).where((cookie) => cookie.name == 'ds_user_id').toList();

  return userCookie.length > 0 ? userCookie : null;
}

vk_setUsername(prefs) async {
  var token = prefs['token'];

  if (token == null) return null;
  Dio dio = new Dio();
  var response = await dio.get(VK_GETUSER_URL(token));

  return response.data['response'];
}

insta_setUsername(prefs) async {
  var token = prefs['insta_username'];

  return token;
}

publishStory(key, mainKey, stories) async {
  var file = await getFile();
  final prefs = jsonDecode(file.readAsStringSync());
  var p6 = false;
  var p12 = false;

  p6 = prefs['p6'];
  p12 = prefs['p12'];
  if (prefs['testP'] < mainKey.currentState.maxTest || p6 || p12) {
    key.currentState.setState(() {
      key.currentState.setLoaderIcon(true);
    });

    for (var story in stories) {
      await uploadStory(await mainKey.currentState.getVkToken(mainKey, prefs), story);
      prefs['testP'] += 1;
      mainKey.currentState.setState(() {
        mainKey.currentState.testP += 1;
      });
    }

    var formatter = new DateFormat('dd-MM-yyyy HH:mm:ss');
    String formatted = formatter.format(DateTime.now());

    prefs['lastPublish'] = key.currentState.stories[key.currentState.stories.length - 1]['taken_at'].toString();
    prefs['lastPublishTime'] = formatted;

    await saveFile(file, prefs);

    key.currentState.setState(() {
      key.currentState.setLoaderIcon(false);
    });
    checkPublishing(key, stories);
  } else {
    //final bool purchased = await billing.purchase('0001');
  }
}

unPublishStory(key, mainKey, stories) async {
  var file = await getFile();
  final prefs = jsonDecode(file.readAsStringSync());
  var formatter = new DateFormat('dd-MM-yyyy HH:mm:ss');
  String formatted = formatter.format(DateTime.now());
  prefs['lastPublish'] = key.currentState.stories[key.currentState.stories.length - 1]['taken_at'].toString();
  prefs['lastPublishTime'] = formatted;
  prefs['lastSkipped'] = prefs['lastPublish'];
  await saveFile(file, prefs);

  checkPublishing(key, stories);
}

publishStoryIsolate(stories, maxTest) async {
  var file = await getFile();
  final prefs = jsonDecode(file.readAsStringSync());
  var p6 = false;
  var p12 = false;

  p6 = prefs['p6'];
  p12 = prefs['p12'];

  if (prefs['testP'] < maxTest || p6 || p12) {
    var vk = prefs['token'];

    for (var story in stories) {
      await uploadStory(vk, story);
      prefs['testP'] += 1;
    }
    var formatter = new DateFormat('dd-MM-yyyy HH:mm:ss');
    String formatted = formatter.format(DateTime.now());

    prefs['lastPublish'] = stories[stories.length - 1]['taken_at'].toString();
    prefs['lastPublishTime'] = formatted;

    await saveFile(file, prefs);
    return 1;
  } else {
    return 0;
  }
}

checkPublishing(key, stories) async {
  var file = await getFile();
  final prefs = jsonDecode(file.readAsStringSync());

  var last = prefs['lastPublish'];
  var time = prefs['lastPublishTime'];

  if (stories is List) {
    try {
      key.currentState.setState(() {
        if (stories[stories.length - 1]['taken_at'].toString() == last) {
          if (last == prefs['lastSkipped']) {
            key.currentState.checkPublishingStr = 'Пропущено ' + time;
          } else {
            key.currentState.checkPublishingStr = 'Публиковалось ' + time;
          }
        } else {
          key.currentState.checkPublishingStr = 'Не публиковалось';
        }
      });
    } catch (e) {}
    ;
  }
}

updateAuto(key) async {
  var file = await getFile();
  final prefs = jsonDecode(file.readAsStringSync());

  var auto = prefs['auto'];

  if (auto == null) {
    auto = false;
  }
  key.currentState.setState(() {
    key.currentState.auto = auto;
  });
  return auto;
}

getFile() async {
  Directory appDocDirectory = await getApplicationDocumentsDirectory();

  File file = new File(appDocDirectory.path + "./vars.txt");
  var exists = await file.exists();
  if (!exists) {
    file.createSync(recursive: true);
    await file.writeAsString(jsonEncode({'auto': false, 'p6': false, 'p12': false, 'testP': 0}));
    return file;
  } else {
    return file;
  }
}

clearFile(file) async {
  await file.writeAsString(jsonEncode({'auto': false, 'p6': false, 'p12': false, 'testP': 0}));
}

getFile2() async {
  Directory appDocDirectory = await getApplicationDocumentsDirectory();

  File file = new File(appDocDirectory.path + "./vars2.txt");

  if (!(await file.exists())) {
    file.createSync(recursive: true);
    await file.writeAsString(jsonEncode({'array': []}));
  }
  return file;
}

getFile3() async {
  Directory appDocDirectory = await getApplicationDocumentsDirectory();

  File file = new File(appDocDirectory.path + "./vars3.txt");

  if (!(await file.exists())) {
    file.createSync(recursive: true);
    await file.writeAsString(jsonEncode({'array': []}));
  }
  return file;
}

saveFile(file, prefs) async {
  await file.writeAsString(jsonEncode(prefs));
}

checkConnection(key) {
  new Timer.periodic(Duration(seconds: 2), (timer) async {
    Connectivity().checkConnectivity().then((result) {
      if (result != ConnectivityResult.none) {}
      key.currentState.setState(() {
        key.currentState.noConnection = false;
      });
      timer.cancel();
    });
  });
}
