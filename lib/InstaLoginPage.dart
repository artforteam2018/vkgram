import 'package:flutter/material.dart';
import 'api.dart';

class InstaLoginPage extends StatefulWidget {
  InstaLoginPage({Key key, this.mainKey}) : super(key: key);

  final mainKey;

  @override
  _InstaLoginPageState createState() => new _InstaLoginPageState();
}

class _InstaLoginPageState extends State<InstaLoginPage> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  var logged = '';
  var loggedResponse;
  var challengeRequired = true;
  var challengeAnswer = false;

  showAlert(path, formData, data, mainKey, showAlert, oldLogin) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Подтверждение"),
          content: new Text("Подтвердите вход через приложение Instagram или почту, затем нажмите ОК"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("ОК"),
              onPressed: () async {
                var result = await challenge(path, formData);
                if (result) {
                  Navigator.of(context).pop();
                  logged = await loginAgain(data, mainKey, showAlert, oldLogin);
                  widget.mainKey.currentState.setState(() {
                    if (logged == 'Успешно') {
                      widget.mainKey.currentState.instaNeedAuth = false;
                      widget.mainKey.currentState.instaAuth = true;
                      if (!widget.mainKey.currentState.vkAuth) {
                        widget.mainKey.currentState.flutterWebviewPlugin.launch(widget.mainKey.currentState.vkUrl);
                      }
                    }
                  });
                }
              },
            ),
          ],
        );
      },
    );
  }

  @override
  initState() {
    super.initState();

    clearCookies();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      onSaved: (String value) {
        widget.mainKey.currentState.setLoginData(value, null);
      },
      decoration: InputDecoration(
        hintText: 'Email или логин:',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      obscureText: true,
      onSaved: (String value) {
        widget.mainKey.currentState.setLoginData(null, value);
      },
      decoration: InputDecoration(
        hintText: 'Пароль:',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
              style: BorderStyle.solid,
            )),
        onPressed: (() async {
          _formKey.currentState.save();
          setState(() {
            logged = 'loading';
          });
          logged = await login(widget.mainKey.currentState.getLoginData(), widget.mainKey, showAlert);
          widget.mainKey.currentState.setState(() {
            if (logged == 'Успешно') {
              widget.mainKey.currentState.instaNeedAuth = false;
              widget.mainKey.currentState.instaAuth = true;

              if (!widget.mainKey.currentState.vkAuth) {
                widget.mainKey.currentState.vkNeedAuth = true;
                widget.mainKey.currentState.flutterWebviewPlugin.launch(widget.mainKey.currentState.vkUrl);
                widget.mainKey.currentState.flutterWebviewPlugin
                    .resize(Rect.fromLTWH(0.05, 0.05, MediaQuery.of(context).size.width, MediaQuery.of(context).size.height * 0.86));
              }
            }
          });

          //widget.mainKey.currentState.setState(()=>'');
          // Navigator.of(context).pushNamed(HomePage.tag);
        }),
        padding: EdgeInsets.all(12),
        color: Colors.white,
        child: Text('Войти', style: TextStyle(color: Colors.black)),
      ),
    );

    final loader = logged == 'loading' || logged == null
        ? Image.asset(
      'assets/loader.gif',
      height: 16,
    )
        : SizedBox(
        height: 16,
        child: Text(
          logged,
          textAlign: TextAlign.center,
        ));

    final stepLabel = Text(
      'Шаг 1',
      style: TextStyle(color: Colors.black54, fontSize: 14),
      textAlign: TextAlign.center,
    );

    final infoLabel = Container(
      child: Text(
        'Внимание!\n'
            'Все данные сохраняются только на вашем устройстве. При переустановке приложения авторизироваться нужно будет заново.',
        style: TextStyle(color: Colors.black, fontSize: 14),
      ),
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(border: Border.all(style: BorderStyle.solid)),
    );

    final forgotLabel = Text(
      'Авторизация в Instagram',
      style: TextStyle(color: Colors.black54, fontSize: 24),
      textAlign: TextAlign.center,
    );

    return new Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height / 24),
              logo,
              SizedBox(height: MediaQuery.of(context).size.height / 24),
              stepLabel,
              forgotLabel,
              SizedBox(height: MediaQuery.of(context).size.height / 12),
              email,
              SizedBox(height: MediaQuery.of(context).size.height / 64),
              password,
              SizedBox(height: MediaQuery.of(context).size.height / 64),
              loader,
              loginButton,
              infoLabel
            ],
          ),
        ),
      ),
    );
  }
}
