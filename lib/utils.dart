import 'package:uuid/uuid.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

generate_uuid(len, hex) {
  var uuid = new Uuid();
  var uuidv1 = uuid.v1();
  if (hex) uuidv1 = uuidv1.replaceAll('-','');

  return uuidv1.substring(0,len);
}

generate_adid(len, hex, modified_seed) {

  var sha = sha256.convert(utf8.encode(modified_seed)).toString();
  var md = md5.convert(utf8.encode(sha)).toString();

  var uuid = new Uuid();

  return uuid.unparse(uuid.parse(md));
}

getHmac(signature, data) {
  var key = ascii.encode(signature);
  var bytes = ascii.encode(data);

  var hmacSha256 = new Hmac(sha256, key); // HMAC-SHA256
  return hmacSha256.convert(bytes);
}

String encodeMap(Map data) {
return data.keys.map((key) => "${Uri.encodeComponent(key)}=${Uri.encodeComponent(data[key])}").join("&");
}